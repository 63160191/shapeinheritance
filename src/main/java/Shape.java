/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Shape {
    protected double x;
    protected double y;
    protected double constant=1;
    
    public Shape(double x,double y){
        this.x=x;
        this.y=y;
        this.constant=constant;
    }
    
    public double calArea(){
        return constant*x*y;
    }
    
    public void print(){
        System.out.println("Shape created");
    }
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getConstant() {
        return constant;
    }
}
