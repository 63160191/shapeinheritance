/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    Triangle(double base,double height){
        super(base,height);
        this.constant=0.5;
    }
    @Override
    public void print(){
        super.print();
        System.out.println("triangle created"); 
        System.out.println("Area of triangle(base = " + getBase() + 
                " height = "+getHeight()+") is " + this.calArea());
    }
    public double getBase() {
        return this.x;
    }

    public double getHeight() {
        return this.y;
    }
}
