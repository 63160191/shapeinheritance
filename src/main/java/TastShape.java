/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TastShape {
    public static void main(String[] args){
         
        Circle circle = new Circle(4);
        circle.print();
        
        Triangle triangle = new Triangle(5,4);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(5,9);
        rectangle.print();
        
        Square square = new Square(6);
        square.print();
        
        System.out.println("--------------------------");
        
        Shape[] shape = {circle,triangle,rectangle,square};
        for(int i=0;i<shape.length;i++){
            if(shape[i] instanceof Circle){
                Circle c = (Circle)shape[i];
                c.print();
            }else if(shape[i] instanceof Triangle){
                Triangle t = (Triangle)shape[i];
                t.print();
            }else if(shape[i] instanceof Rectangle){
                Rectangle r = (Rectangle)shape[i];
                r.print();
            }else if(shape[i] instanceof Square){
                Square s = (Square)shape[i];
                s.print();
            }
        }
    }
}
