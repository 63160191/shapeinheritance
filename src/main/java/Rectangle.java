/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Rectangle extends Shape{
    double width;
    private double height;
    
    Rectangle(double width,double height){
        super(width,height);
        this.width=width;
        this.height=height;
    }
    @Override
    public double calArea(){
        return width*height;
    }

    @Override
    public void print(){
        super.print();
        System.out.println("rectangle created"); 
        System.out.println("Area of rectangle(width = " + getWidth() + 
                " height = "+getHeight()+") is " + this.calArea());
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
}
